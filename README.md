Module Insectopia Custom Combat pour Foundry VTT

Ce module est une adaptation du Custom Tracker de Foundry pour Insectopia.
Il nécessite l'installation d'Insectopia développé avec World Building.

Rejoignez la communauté Discord FR : <a href='https://discord.gg/pPSDNJk'>Foundry VTT Discord FR</a>

Ce module est développé par Kristov.

----------------
Fonctionnalités 
----------------

- Tirage de l'initiative
- Compendium avec 2 macros : 
	Lancer l'initiative
	Tirer du sac
- Combat Tracker
	MJ : Tirage de l'initiative individuelle
	MJ : Tirage pour tous les PJs
	MJ : Tirage pour tous les PNJs
	Clic droit pour éditer les différents niveaux de couleur
	Affichage des combattants par ordre de couleur
	Initiative : nombre d'actions dans le tour, qui est mise à jour à chaque action
	Utiliser le bouton Tour suivant pour décrémenter l'action du premier joueur et mettre à jour le Combat Tracker
	Un clic sur Tour suivant une fois que toutes les actions ont été réalisées passe au Round suivant

Limitations
-----------

- Pas d'historique : ne pas utiliser le bouton de retour arrière
- Message d'erreur si le joueur clique sur le dé "jet d'initiative" dans le Combat Tracker. C'est réservé au MJ.

Release Notes
-------------

0.3.0
- Prise en compte du statut Hors-Combat

0.2.0
- Correction de l'affichage côté PJ