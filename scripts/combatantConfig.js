export default class InsectopiaCombatantConfig extends CombatantConfig {

    get template() {
        return "modules/insectopia/templates/combatant-config.hbs";
    }

}