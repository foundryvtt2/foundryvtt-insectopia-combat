import { drawInitiativeForCombatant } from "./initiative.js";
import { displayMessage } from "./initiative.js";
import { displayMessageInitiative } from "./initiative.js";
import { compareCombatantRedColor, compareCombatantGreenColor, compareCombatantBlueColor, compareCombatantWhiteColor, compareCombatantBlackColor} from "./utils.js"

export default class InsectopiaCombat extends Combat {

    _sortCombatants(a, b) {

        if (a.defeated){
            return 1;
        }
        if (b.defeated){
            return -1;
        }
        if (a.flags.initRed != 0 || b.flags.initRed !=0){
            return compareCombatantRedColor(a, b);
        }
        if (a.flags.initGreen != 0 || b.flags.initGreen !=0){
            return compareCombatantGreenColor(a, b);
        }
        if (a.flags.initBlue != 0 || b.flags.initBlue !=0){
            return compareCombatantBlueColor(a, b);
        }
        if (a.flags.initWhite != 0 || b.flags.initWhite !=0){
            return compareCombatantWhiteColor(a, b);
        }
        if (a.flags.initBlack != 0 || b.flags.initBlack !=0){
            return compareCombatantBlackColor(a, b);
        }

        return a.tokenId - b.tokenId;
    }

    _prepareCombatant(c, scene, players, settings = {}){
        let combatant = super._prepareCombatant(c, scene, players, settings);    
        combatant.flags.initRed = Number.isNumeric(combatant.flags.initRed) ?  Number(combatant.flags.initRed) : 0;
        combatant.flags.initGreen = Number.isNumeric(combatant.flags.initGreen) ?  Number(combatant.flags.initGreen) : 0;
        combatant.flags.initBlue = Number.isNumeric(combatant.flags.initBlue) ?  Number(combatant.flags.initBlue) : 0;
        combatant.flags.initWhite = Number.isNumeric(combatant.flags.initWhite) ?  Number(combatant.flags.initWhite) : 0;
        combatant.flags.initBlack = Number.isNumeric(combatant.flags.initBlack) ?  Number(combatant.flags.initBlack) : 0;

        combatant.flags.nbActions = combatant.flags.initGreen + combatant.flags.initRed + combatant.flags.initBlue + combatant.flags.initWhite + combatant.flags.initBlack;

        if (combatant.flags.nbActions !== 0){
            combatant.initiative = combatant.flags.nbActions;
        }
        else combatant.initiative = null;

        return combatant;
    }

    async rollInitiative(ids, {formula=null, updateTurn=true, messageOptions={}}={}) {

    // Structure input data
    ids = typeof ids === "string" ? [ids] : ids;
    const currentId = this.combatant._id;

    // Iterate over Combatants, performing an initiative roll for each
    const [updates, messages] = ids.reduce((results, id, i) => {
        let [updates, messages] = results;

        // Get Combatant data
        const c = this.getCombatant(id);
        if ( !c || !c.owner ) return results;

        // Roll initiative
        const draw = drawInitiativeForCombatant(c);
        console.log("Initiative triée pour le combattant " + c.name + " : " + draw);
       let initRed = 0;
       let initGreen = 0;
       let initBlue = 0;
       let initWhite = 0;
       let initBlack = 0;
       let nbActions = 0;
        for (let index = 0; index < draw.length; index++) {
            const element = draw[index];
            switch (element) {
                case "red":
                   initRed += 1;
                    break;
                case "green":
                   initGreen += 1;
                    break;
                case "blue":
                   initBlue += 1;
                    break;
                case "white":
                   initWhite += 1;
                    break;
                case "black":
                   initBlack += 1;
                    break;                
                default:
                    break;
            }
           nbActions += 1;
        }
        updates.push({_id: id, "initiative":nbActions, "flags.nbActions":nbActions, "flags.initRed":initRed, "flags.initGreen":initGreen, "flags.initBlue":initBlue, "flags.initWhite":initWhite, "flags.initBlack":initBlack});

    // Construct chat message data   
        let messageData = mergeObject({
            speaker: {
                scene: canvas.scene._id,
                actor: c.actor ? c.actor._id : null,
                token: c.token._id,
                alias: c.token.name
            },
            flavor: `${c.token.name} tire l'Initiative!`,
            content: draw,
            type: CONST.CHAT_MESSAGE_TYPES.OTHER
        }, messageOptions);
        //messageData.content = draw;     

        messages.push(messageData);

        return results;
    }, [[], []]);

    if ( !updates.length ) return this;

    // Update multiple combatants
    await this.updateEmbeddedEntity("Combatant", updates);

    // Update the content display in the chat
    for (let index = 0; index < messages.length; index++) {
        let element = messages[index];
        element.content = await renderTemplate("/modules/insectopia/templates/display-initiative.html", {results: element.content} );
    }
    
    // Create multiple chat messages 
    await CONFIG.ChatMessage.entityClass.create(messages);

    // Return the updated Combat
    // return this;
    
    return this.update({turn: 0});

    }

    async startCombat(){
        await this.setupTurns();
        return super.startCombat();
    }

    async nextTurn(){
        let combatant = this.combatant;

        if (combatant.initiative <= 0)    {
            return this.nextRound();
        }    

        await this.spendAction(combatant);
        return this.update({turn: 0});
    }

    /**
     * Spend 1 action starting from higher rank (red then green then blue then white then black)
     * @param {*} combatant 
     */
    async spendAction(combatant) {
        let newInitiative = combatant.initiative - 1;
        const nbActionsLeft = combatant.flags.nbActions - 1;

        let newColorValue;

        if (combatant.flags.initRed > 0) {
            newColorValue =  combatant.flags.initRed - 1;  
            return this.updateCombatant({
                _id: combatant._id,
                initiative: newInitiative,
                ["flags.initRed"]: newColorValue,
                ["flags.nbActions"]: nbActionsLeft
            }); 
        }
        
        if (combatant.flags.initGreen > 0) {
            newColorValue =  combatant.flags.initGreen - 1;   
            return this.updateCombatant({
                _id: combatant._id,
                initiative: newInitiative,
                ["flags.initGreen"]: newColorValue,
                ["flags.nbActions"]: nbActionsLeft
            });
        }
        
        if (combatant.flags.initBlue> 0) {
            newColorValue =  combatant.flags.initBlue - 1;   
            return this.updateCombatant({
                _id: combatant._id,
                initiative: newInitiative,
                ["flags.initBlue"]: newColorValue,
                ["flags.nbActions"]: nbActionsLeft
            });
        }
        
        if (combatant.flags.initWhite> 0) {
            newColorValue =  combatant.flags.initWhite - 1;   
            return this.updateCombatant({
                _id: combatant._id,
                initiative: newInitiative,
                ["flags.initWhite"]: newColorValue,
                ["flags.nbActions"]: nbActionsLeft
            });
        }
        
        if (combatant.flags.initBlack> 0) {
            newColorValue =  combatant.flags.initBlack - 1;   
            return this.updateCombatant({
                _id: combatant._id,
                initiative: newInitiative,
                ["flags.initBlack"]: newColorValue,
                ["flags.nbActions"]: nbActionsLeft
            });
        }    
    }


  /**
   * Reset all combatant initiative scores, setting the turn back to zero
   * @return {Promise<Combat>}
   */
  async resetAll() {
    const updates = this.data.combatants.map(c => { return {
      _id: c._id,
      initiative: null,
      "flags.initRed": 0,
      "flags.initGreen": 0,
      "flags.initBlue": 0,
      "flags.initWhite": 0,
      "flags.initBlack": 0,
      "flags.nbActions": 0
    }});
    await this.updateEmbeddedEntity("Combatant", updates);
    return this.update({turn: 0});
  }

}