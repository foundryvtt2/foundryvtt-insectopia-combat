
import { pull } from "./utils.js"

export async function drawFromPouch() {

let displayContent = "";
let opposition = -1;

let users = game.users.filter(user => user.active);
let checkOptions = ""
let playerTokenIds = users.map(u => u.character?.id).filter(id => id !== undefined);
let selectedPlayerIds = canvas.tokens.controlled.map(token => {
  if (playerTokenIds.includes(token.actor.id)) return token.actor.id;
});

// Build checkbox list for all active players
users.forEach(user => {
  checkOptions+=`
    <br>
    <input type="checkbox" name="${user.id}" value="${user.name}">\n
    <label for="${user.id}">${user.name}</label>
  `
});

displayContent = `Chuchoter à: ${checkOptions}`;

let d = new Dialog({ 
  title: "Tirage de blattes - Choisir l'opposition", 
  content: displayContent, 
  buttons: {    
    zero: { label: "0", callback: () => opposition = 0},
    one: { label: "1", callback: () => opposition = 1},
    two: { label: "2", callback: () => opposition = 2},
    three: { label: "3", callback: () => opposition = 3},
    four: { label: "4", callback: () => opposition = 4},
    five: { label: "5", callback: () => opposition = 5},
    six: { label: "6", callback: () => opposition = 6},
  },
  default: "zero",
  close: html => {
      if (opposition != -1){
        if (opposition != 1){
          displayMessage(users,html,pullWithOpposition(opposition));  
        }
        else {
          const tirage = pullWithOpposition(1);
          displayMessage(users,html,tirage.slice(0,1));
          let d2 = new Dialog({
            title: "Garder ?",
            content: "",
            buttons: {
              no: {
                icon: "<i class='fas fa-check'></i>",
                label: `Je retire`,
                callback: () => displayMessage(users,html,tirage.slice(1,2))
              },
              yes: {
              icon: "<i class='fas fa-times'></i>",
              label: `Je conserve`
              }
            }
          }).render(true); 
        }
      }      
    }
}).render(true);

}

function pullWithOpposition(opposition){
    console.log(`Opposition = ${opposition} blatte(s)`);

    let number = 1;

    // Opposition 1, tirer 2 blattes
    if (opposition === 1){
        number = 2;
    }
    else if (opposition > 1){
        number = opposition;
    }

    const results = pull(number);

    console.log(`Résultat du tirage : ${results}`);
    return results;
}

function displayMessage(users,html,tirage){
  var targets = [];
  // build list of selected players ids for whispers target
  for ( let user of users ) {
    if (html.find('[name="'+user.id+'"]')[0].checked){
      targets.push(user.id);
    }
  }
  console.log(targets);

  //console.log("Tirage = " + tirage);
  let content = '<div class="row"> <div class="column">';
  for (let i = 0; i < tirage.length ; i++){
    const color = tirage[i];
    const image = `modules/insectopia/images/cockroach-${color}.jpg`;
    content += `<img src="${image}" alt="" height="62px" width="43px">`;
  }
  content += "</div></div>";
  //console.log("content = " + content);
  const chatData = {
    user: game.user._id,
    speaker: game.user,
    content: content,
    whisper: targets,
    type: CONST.CHAT_MESSAGE_TYPES.OTHER
  };
  ChatMessage.create(chatData, {});
}