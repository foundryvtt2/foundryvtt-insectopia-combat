
const pouch = ["red","red","red","green","green","green","green","green","green","blue","blue","blue","blue","blue","blue","blue","blue","blue","blue","blue","blue","white","white","white","white","white","white","white","white","white","white","white","white","white","white","white","white","white","white","black","black","black"];

/**
 * Returns an int depending from a color
 * @param {*} color 
 */
const colorToInt = (color) => {
    switch(color) {
        case "red":
          return 1;
          break;
        case "green":
          return 2;
          break;
        case "blue":
          return 3;
          break;
        case "white":
          return 4;
          break;
        case "black":
          return 5;
          break;          
        default:
          // code block
      } 
}

/**
 * Sort Color : Red then Green then Blue then white then Black
 * @param {*} a 
 * @param {*} b 
 */
const sortColors = (a, b) => {
    const intA = colorToInt(a);
    const intB = colorToInt(b);
    return intA - intB;
}

/**
 * Pull nbDraw from Pouch
 * @param {*} nbDraw 
 */
export const pull = function(nbDraw){

    console.log("Tirage de " + nbDraw + " blattes");
    const newpouch = pouch.slice();
    let results = [];
    for (let i = 0; i < nbDraw; i++) {
        // Tirage entre 1 et le nombre d'éléments dans le sac
        let draw = Math.floor(Math.random()*(newpouch.length-1));
        //console.log("Draw : " + draw);
        results.push(newpouch[draw]);
        newpouch.splice(draw,1);
    }

    //console.log("Newpouch = " + newpouch);
    console.log(`Résultat du tirage : ${results}`);
    return results.sort(sortColors);
};

export function compareCombatantRedColor(a, b) {
  if (a.flags.initRed > 0 && b.flags.initRed == 0){
      return -1;
  }
  if (b.flags.initRed > 0 && a.flags.initRed == 0){
      return 1;
  }
  if (a.flags.initRed > 0 && b.flags.initRed > 0){
      return a.flags.initRed - b.flags.initRed;
  }
}

export function compareCombatantGreenColor(a, b) {
  if (a.flags.initGreen > 0 && b.flags.initGreen == 0){
      return -1;
  }
  if (b.flags.initGreen > 0 && a.flags.initGreen == 0){
      return 1;
  }
  if (a.flags.initGreen > 0 && b.flags.initGreen > 0){
      return a.flags.initGreen - b.flags.initGreen;
  }
}

export function compareCombatantBlueColor(a, b) {
  if (a.flags.initBlue > 0 && b.flags.initBlue == 0){
      return -1;
  }
  if (b.flags.initBlue > 0 && a.flags.initBlue == 0){
      return 1;
  }
  if (a.flags.initBlue > 0 && b.flags.initBlue > 0){
      return a.flags.initBlue - b.flags.initBlue;
  }
}

export function compareCombatantWhiteColor(a, b) {
  if (a.flags.initWhite > 0 && b.flags.initWhite == 0){
      return -1;
  }
  if (b.flags.initWhite > 0 && a.flags.initWhite == 0){
      return 1;
  }
  if (a.flags.initWhite > 0 && b.flags.initWhite > 0){
      return a.flags.initWhite - b.flags.initWhite;
  }
}

export function compareCombatantBlackColor(a, b) {
  if (a.flags.initBlack > 0 && b.flags.initBlack == 0){
      return -1;
  }
  if (b.flags.initBlack > 0 && a.flags.initBlack == 0){
      return 1;
  }
  if (a.flags.initBlack > 0 && b.flags.initBlack > 0){
      return a.flags.initBlack - b.flags.initBlack;
  }
}