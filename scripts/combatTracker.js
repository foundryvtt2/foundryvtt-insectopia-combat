import InsectopiaCombatantConfig from "./combatantConfig.js";

export default class InsectopiaCombatTracker extends CombatTracker {

    get template(){
        return "modules/insectopia/templates/combat-tracker.hbs";
    }

    /** @override */          
    _onConfigureCombatant(li) {
        const combatant = this.combat.getCombatant(li.data('combatant-id'));
        new InsectopiaCombatantConfig(combatant, {
            top: Math.min(li[0].offsetTop, window.innerHeight - 350),
            left: window.innerWidth - 720,
            width: 400
        }).render(true);
    }

    /** @override */
    activateListeners(html){
      
        super.activateListeners(html);
    
        html.find(".initiativeRed").change(this._onInitiativeRedChanged.bind(this));
        html.find(".initiativeGreen").change(this._onInitiativeGreenChanged.bind(this));
        html.find(".initiativeBlue").change(this._onInitiativeBlueChanged.bind(this));
        html.find(".initiativeWhite").change(this._onInitiativeWhiteChanged.bind(this));
        html.find(".initiativeBlack").change(this._onInitiativeBlackChanged.bind(this));
      
    }
    
    async _onInitiativeRedChanged(event){
        const input = event.currentTarget;
        const li = input.closest(".combatant");
        const c = this.combat.getCombatant(li.dataset.combatantId);

        const newInitiative = c.flags.initRed + c.flags.initGreen + c.flags.initBlue + c.flags.initWhite + c.flags.initBlack;
        await this.combat.updateCombatant({_id: c._id, ["flags.initRed"]: input.value, initiative: newInitiative, ["flags.nbActions"]: newInitiative});

        this.render();
    }

    async _onInitiativeGreenChanged(event){
        const input = event.currentTarget;
        const li = input.closest(".combatant");
        const c = this.combat.getCombatant(li.dataset.combatantId);

        const newInitiative = c.flags.initRed + c.flags.initGreen + c.flags.initBlue + c.flags.initWhite + c.flags.initBlack;
        await this.combat.updateCombatant({_id: c._id, ["flags.initGreen"]: input.value, initiative: newInitiative, ["flags.nbActions"]: newInitiative});

        this.render();
    }

    async _onInitiativeBlueChanged(event){
        const input = event.currentTarget;
        const li = input.closest(".combatant");
        const c = this.combat.getCombatant(li.dataset.combatantId);

        const newInitiative = c.flags.initRed + c.flags.initGreen + c.flags.initBlue + c.flags.initWhite + c.flags.initBlack;
        await this.combat.updateCombatant({_id: c._id, ["flags.initBlue"]: input.value, initiative: newInitiative, ["flags.nbActions"]: newInitiative});

        this.render();
    }

    async _onInitiativeWhiteChanged(event){
        const input = event.currentTarget;
        const li = input.closest(".combatant");
        const c = this.combat.getCombatant(li.dataset.combatantId);

        const newInitiative = c.flags.initRed + c.flags.initGreen + c.flags.initBlue + c.flags.initWhite + c.flags.initBlack;
        await this.combat.updateCombatant({_id: c._id, ["flags.initWhite"]: input.value, initiative: newInitiative, ["flags.nbActions"]: newInitiative});

        this.render();
    }

    async _onInitiativeBlackChanged(event){
        const input = event.currentTarget;
        const li = input.closest(".combatant");
        const c = this.combat.getCombatant(li.dataset.combatantId);

        const newInitiative = c.flags.initRed + c.flags.initGreen + c.flags.initBlue + c.flags.initWhite + c.flags.initBlack;
        await this.combat.updateCombatant({_id: c._id, ["flags.initBlack"]: input.value, initiative: newInitiative, ["flags.nbActions"]: newInitiative});

        this.render();
    }


  /** @override */
  async _onToggleDefeatedStatus(c) {
    console.log("INSECTOPIA CUSTOM MODULE TOGGLE DEFEAT");
    let isDefeated = !c.defeated;

    if (isDefeated){
        await this.combat.updateCombatant({_id: c._id, defeated: isDefeated, initiative: 0, ["flags.nbActions"]: 0, ["flags.initGreen"]: 0, ["flags.initBlue"]: 0, ["flags.initWhite"]: 0, ["flags.initBlack"]: 0});
    }
    else {
        await this.combat.updateCombatant({_id: c._id, defeated: isDefeated});
    }

    const token = canvas.tokens.get(c.tokenId);
    if ( !token ) return;

    // Push the defeated status to the token
    let status = CONFIG.statusEffects.find(e => e.id === CONFIG.Combat.defeatedStatusId);
    let effect = token.actor && status ? status : CONFIG.controlIcons.defeated;
    await token.toggleEffect(effect, {overlay: true, active: isDefeated});
  }
}