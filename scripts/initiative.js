import { pull } from "./utils.js"

export async function displayMessage(results, {messageData={}, messageOptions={}}={}){
  const speaker = ChatMessage.getSpeaker({user: game.user});

  // Construct chat data
  messageData = mergeObject({
    flavor: `Initiative`,
    user: game.user._id,
    speaker: speaker,
    type: CONST.CHAT_MESSAGE_TYPES.OTHER,
    flags: {"insectopia.messageType": "initiative"}
  }, messageData);

  // Render the chat card
  messageData.content = await renderTemplate("/modules/insectopia/templates/display-initiative.html", {results: results} );

  // Create the chat message
  return ChatMessage.create(messageData, messageOptions);
};

export async function displayMessageInitiative(draw){
  const html = await renderTemplate("/modules/insectopia/templates/display-initiative.html", {results: draw});
  return html;
}

export async function drawInitiative(){
  //let target = Array.from(game.user.targets)[0];
  let token = canvas.tokens.controlled[0];
  let actorD = token.actor || game.user.character;
  //let actorD = actor || canvas.tokens.controlled[0].actor || game.user.character;
  const initiative = actorD.data.data.attributes.initiative.value;
  console.log(`L'acteur ${actorD.name} a ${initiative} pour initiative`);

  let draw = pull(initiative);
  
  await displayMessage(draw);  
}

export function drawInitiativeForCombatant(combatant){
  const initiative = combatant.actor.data.data.attributes.initiative.value;
  console.log(`L'acteur ${combatant.name} a ${initiative} pour initiative`);

  let draw = pull(initiative);
  
  //await displayMessage(draw, {messageData: {speaker: combatant.name}});
  //await displayMessage(draw);
  return draw;
}
