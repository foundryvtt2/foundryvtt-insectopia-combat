
import { drawInitiative } from "./initiative.js";
import { drawFromPouch } from "./macro.js"

import InsectopiaCombat from "./combat.js";
import InsectopiaCombatTracker from "./combatTracker.js";

Hooks.once("init", () => {

    CONFIG.Combat.entityClass = InsectopiaCombat;
    CONFIG.ui.combat = InsectopiaCombatTracker;

    // Allows to have macros called game.insectopia.tirerInitiative() and game.insectopia.tirerOpposition() {

    game.insectopia = {
        tirerInitiative() {
            drawInitiative();
        },
        tirerOpposition() {
            drawFromPouch();
        }
    };

});

Hooks.on('createCombatant', (combat, combatantId, options) => {
    combatantId.initiative = null;
});